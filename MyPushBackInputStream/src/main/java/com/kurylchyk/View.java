package com.kurylchyk;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    private Map<String, String> menu;
    private Controller controller = new Controller();

    View(){
        menu = new HashMap<String, String>();
        menu.put("1 - ","1 - Read");
        menu.put("2 - ","2 - Push back the symbol");
        menu.put("3 - ","3 - Push back the String");
        menu.put("4 - ","4 - Show available to read");
        menu.put("Q - ","Q - exit");
        choose();
    }

    public  void showMenu(){
        for(Object element: menu.values()){
            System.out.println(element);
        }
    }

    public void choose(){


        Scanner sc = new Scanner(System.in);
        String key ="" ;

        do{
            showMenu();
            System.out.print("Choose the option: ");
            key = sc.nextLine();

            switch(key){
                case "1":
                    System.out.println(controller.read());break;
                case "2":controller.pushFront();break;
                case "3":controller.unread();break;
                case "4":controller.getAvailable();break;
            }

        }while(key.toUpperCase()!="Q");

    }
}
