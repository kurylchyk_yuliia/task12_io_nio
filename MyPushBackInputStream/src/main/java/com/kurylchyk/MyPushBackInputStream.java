package com.kurylchyk;

import java.io.IOException;
import java.io.InputStream;

public class MyPushBackInputStream extends InputStream {

    protected byte[] buf;
    protected int pos;



    public int read() throws IOException {
        if(pos==buf.length)
            return -1;
        else
            return buf[pos++];

    }

    MyPushBackInputStream(byte[] in) {
        pos = 0;
        buf = in;
    }


    public void unread(int b){
        byte[] temp = new byte[buf.length+1];

        for(int index = 0; index<buf.length; index++){
            temp[index+1] = buf[index];
        }
        temp[0] = (byte)b;
        buf = temp;
    }

    public void unread(byte[] b) {

        byte[] temp = new byte[b.length+buf.length];
        int index = 0;
        for(; index<b.length; index++){
            temp[index] = b[index];
        }
        int i = 0;
        for(;i<buf.length;index++,i++){

            temp[index] = buf[i];
        }
        buf = temp;
    }

    public void  availableToRead(){

        for(byte element: buf){
            System.out.print((char)element);
        }
        System.out.println();
    }
}
