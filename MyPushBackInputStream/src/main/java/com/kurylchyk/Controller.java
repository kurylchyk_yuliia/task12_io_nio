package com.kurylchyk;

import java.io.IOException;
import java.util.Scanner;

public class Controller {

    private static Scanner sc;
    private String str;
    private MyPushBackInputStream myPushBack;

    public Controller() {
        sc = new Scanner(System.in);
        System.out.println("Enter the line: ");
        str = sc.nextLine();
        myPushBack = new MyPushBackInputStream(str.getBytes());
    }

    public String read()  {

        String returned = "";
        int element = 0;
        try{
        while (true) {
            element = myPushBack.read();
            if (element == -1) break;
            returned+=(char)element;
        }
        } catch (IOException ex){
            ex.printStackTrace();
        }
        return  returned;
    }

    public void pushFront(){
        System.out.println("Enter the byte:");
        String temp = sc.nextLine();
        int back = temp.charAt(0);
        myPushBack.unread(back);
    }

    public void unread(){
        System.out.println("Enter the line :");
        str = sc.nextLine();
        myPushBack.unread(str.getBytes());
    }

    public void getAvailable(){
        myPushBack.availableToRead();
    }
}
