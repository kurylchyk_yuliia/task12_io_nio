package com.kurylchyk;

import java.io.IOException;

public class Application {

    public static void main(String[] args) throws IOException {

       Controller controller= new Controller(args[0]);
       controller.showComments();

    }
}
