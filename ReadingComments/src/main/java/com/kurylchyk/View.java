package com.kurylchyk;

@FunctionalInterface

public interface View {

    void Show(String info);
}
