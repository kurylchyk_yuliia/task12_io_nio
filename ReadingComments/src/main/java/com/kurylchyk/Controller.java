package com.kurylchyk;

import java.io.IOException;

public class Controller {

    private JavaComments jc ;
    private View view;

    Controller(String filename) throws IOException {
        jc = new JavaComments(filename);
        view = (s)-> {System.out.println(s);};
    }

    public void showComments(){
        view.Show(jc.showAllComments());
    }

}
