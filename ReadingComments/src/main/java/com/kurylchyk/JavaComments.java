package com.kurylchyk;

import java.io.*;

public class JavaComments {

    private String fileName;
    private String allCode = "";
    private String allComments = "";

    JavaComments(String name) throws IOException {
        fileName = name+".txt";
        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(fileName));
        int b = buf.read();
        allCode += (char) b;
        try {
            while (b != -1) {
                b = buf.read();
                allCode += (char) b;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        buf.close();

        findComments();;
    }
    private void findComments(){

        for(int index = 0; index< allCode.length(); index++) {

            if(allCode.charAt(index)=='/' &&allCode.charAt(index+1)=='*') {

                int currentIndex = index;
                while(true){

                    if(allCode.charAt(currentIndex)=='*' && allCode.charAt(currentIndex+1)=='/') {
                        allComments+="*/\n";
                        break;
                    }
                    allComments+=allCode.charAt(currentIndex);
                    currentIndex++;
                }
                index = currentIndex;
            }
        }
    }
    public String showAllComments(){

        return allComments;
    }
}
