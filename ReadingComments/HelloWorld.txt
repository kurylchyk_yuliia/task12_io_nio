/**
 * Class Hello world that  displays "Hello world"
 */

public class HelloWorld {

    /**
     * str1  contains "Hello"
     */
    public static String str1 = "Hello";

    /**
     * str2 contains "world"
     */
    public static String str2 = " world";

    /**
     * method show() displays the str1 and str2
     */
    public static void show() {
        System.out.println(str1+str2);
    }
    
}