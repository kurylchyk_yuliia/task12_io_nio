package com.kurylchyk;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
  private  Map<String, String> menu;
  private WorkingWithFiles workingWithFiles = new WorkingWithFiles();
  private static  Scanner sc = new Scanner(System.in);

    View(){
        menu = new LinkedHashMap<>();
        menu.put("1 - ","Write the file using usual writing");
        menu.put("2 - ","Write the file using buffered writing");
        menu.put("3 - ","Read the file using usual reading");
        menu.put("4 - ","Read the file using buffered reading");
        menu.put("5 - ", "Read the file using appropriate buffer size");
        menu.put("Q - ","exit");
        showMenu();
        chooseOption();
    }

    public void showMenu(){
        for(String element: menu.values()){
            System.out.println(element);
        }
    }


    public void chooseOption() {

        String key="";


       do{
            System.out.println("Choose the option");
            key = sc.nextLine();
            try{
            switch (key) {
                case "1":
                    workingWithFiles.writerUsual();
                    break;
                case "2":
                    workingWithFiles.writerBuffered();
                    break;
                case "3":
                    workingWithFiles.readerUsual();
                    break;
                case "4":
                    workingWithFiles.readerBuffered();
                case "5":
                    workingWithFiles.readerBufferedWithSize();
                    break;
            }
            }catch (IOException ex){
                ex.printStackTrace();
            }
        }while(!(key.toUpperCase().compareTo("Q")==0));
    }
}
