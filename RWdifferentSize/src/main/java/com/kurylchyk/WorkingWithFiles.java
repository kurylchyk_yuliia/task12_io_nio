package com.kurylchyk;
import java.io.*;
import  java.util.Random;
import java.util.Scanner;

public class WorkingWithFiles {
    private Random random = new Random();
    private static  Scanner sc = new Scanner(System.in);

    public void readerUsual() throws IOException {
        int count  = 0;
        System.out.println("Usual reading");
        InputStream inputStream = new FileInputStream("1.bmp");
        int data = inputStream.read();
        while(data!=-1){
            data = inputStream.read();
            count++;

        }

        inputStream.close();
        System.out.println("Count = "+count);
        System.out.println("The end of usual reading");
    }


    public void readerBuffered() throws IOException {

        int count = 0;
        System.out.println("Buffered reading");
        DataInputStream input  = new DataInputStream(new BufferedInputStream(new FileInputStream("2.bmp")));

        byte b = input.readByte();
        try{
            while(b!=-1){
                b = input.readByte();
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        input.close();
        System.out.println("Count: "+count);
        System.out.println("The end of buffered reading");
    }

    public void readerBufferedWithSize() throws IOException {

        System.out.println("Enter the size of buffer: ");
        int sizeOfBuffer = sc.nextInt();
        int count = 0;
        System.out.println("Buffered reading with buffer size " +sizeOfBuffer +" bytes");
        DataInputStream input  = new DataInputStream(new BufferedInputStream(new FileInputStream("Image.bmp"),sizeOfBuffer));

        byte b = input.readByte();
        try{
            while(b!=-1){
                b = input.readByte();
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        input.close();
        System.out.println("Count: "+count);
        System.out.println("The end of buffered reading with buffer size " +sizeOfBuffer +" bytes");

    }


    public void writerUsual() throws IOException {
        int count  = 0;
        System.out.println("Usual  writing");
        OutputStream outputStream = new FileOutputStream("1.bmp");
        try{
            int index = 0;
            while(index< 50*1024*1024) {
                outputStream.write(random.nextInt());
                index++;
            }
        } catch (IOException ex){
            System.out.println(ex.getMessage());
        } finally {
            outputStream.close();
        }

        System.out.println("The end of usual writing");
    }

    public void writerBuffered() throws IOException {


        System.out.println("Buffered writing");
        int sizeOfBuffer  = 1024;
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream("2.bmp"));

        try{
            int index = 0;
            while(index< 50*1024*1024){
                bufferedOutputStream.write(random.nextInt());
                index++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            bufferedOutputStream.close();
        }
        System.out.println("The end of buffered writing");
    }

}
