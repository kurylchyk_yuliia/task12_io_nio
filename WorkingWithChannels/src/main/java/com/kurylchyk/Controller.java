package com.kurylchyk;

import java.util.Scanner;

public class Controller {

    private SomeBuffer someBuffer;
    private View view;
    private Scanner sc = new Scanner(System.in);
    Controller(){
        someBuffer = new SomeBuffer();
        view = (s)-> System.out.println(s);

    }

    public void readFile(){
        view.show(someBuffer.readFile());
    }
    public void writeToFile(){
        System.out.println("Enter the string: ");
        String str = sc.nextLine();
        if(someBuffer.writeIntoFile(str)){
            view.show("Completed successfully");
        } else{
            view.show("Smth went wrong");
        }

    }
}
