package com.kurylchyk;

@FunctionalInterface
public interface View {
    void show(String info);
}
