package com.kurylchyk;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.*;
import java.nio.channels.FileChannel;

public class SomeBuffer {

    private FileChannel channel;
    private RandomAccessFile file;
    private ByteBuffer byteBuffer;
    private int position;
    SomeBuffer() {

            byteBuffer = ByteBuffer.allocate(512);
    }


    public String readFile()  {
        try {
            file = new RandomAccessFile("file.txt", "rw");
        }catch(FileNotFoundException e){
            e.getMessage();
        }
       FileChannel channel = file.getChannel();
        String text = "";
        try {
            int b = channel.read(byteBuffer);
            while (b != -1) {
                byteBuffer.flip();

                while (byteBuffer.hasRemaining()) {
                    text += (char) byteBuffer.get();
                }

                position = byteBuffer.position()+1;
                byteBuffer.clear();
                b = channel.read(byteBuffer);
            }

            file.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return text;
    }

    public boolean writeIntoFile(String str)  {
        try {
            file = new RandomAccessFile("file.txt", "rw");
            file.seek(position);
        }catch(IOException e){
            e.getMessage();
        }
       FileChannel channel = file.getChannel();
        try {
            int index = 0;
            while (index <str.length()) {
                byteBuffer.put((byte)str.charAt(index++));
            }
            byteBuffer.flip();
            channel.write(byteBuffer);


            if (byteBuffer.hasRemaining()) {

                byteBuffer.compact();
            } else {

                byteBuffer.clear();
            }

           channel.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
