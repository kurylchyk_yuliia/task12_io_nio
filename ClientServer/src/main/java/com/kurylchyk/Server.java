package com.kurylchyk;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;


public class Server {

    @SuppressWarnings("unused")
    public static void main(String[] args) throws IOException {

        Selector selector = Selector.open();

        ServerSocketChannel Socket = ServerSocketChannel.open();
        InetSocketAddress addr = new InetSocketAddress("localhost", 1111);

        Socket.bind(addr);

        Socket.configureBlocking(false);

        int ops = Socket.validOps();
        SelectionKey selectKy = Socket.register(selector, ops, null);
        String result ="?*?*?";

        while (true) {

            show("i'm a server and i'm waiting for a connection...");

            selector.select();
            Set<SelectionKey> Keys = selector.selectedKeys();
            Iterator<SelectionKey> Iterator = Keys.iterator();

            while (Iterator.hasNext()) {
                SelectionKey myKey = Iterator.next();
                if (myKey.isAcceptable()) {
                    SocketChannel Client = Socket.accept();
                    Client.configureBlocking(false);
                    Client.register(selector, SelectionKey.OP_READ);
                    show("Connection Accepted: " + Client.getLocalAddress() + "\n");
                } else if (myKey.isReadable()) {

                    SocketChannel Client = (SocketChannel) myKey.channel();
                    ByteBuffer Buffer = ByteBuffer.allocate(256);
                    Client.read(Buffer);
                    result = new String(Buffer.array()).trim();

                    show("Message received: " + result);
                }
                Iterator.remove();
            }
        }
    }

    private static void show(String str) {
        System.out.println(str);
    }
}