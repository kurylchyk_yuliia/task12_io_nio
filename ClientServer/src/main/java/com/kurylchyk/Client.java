package com.kurylchyk;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Scanner;

public class Client {
    private static String clientName;
    private static String message;

    public static void main(String[] args) throws IOException, InterruptedException {

        InetSocketAddress addr = new InetSocketAddress("localhost", 1111);
        SocketChannel Client = SocketChannel.open(addr);

        show("Connecting to Server on port 1111...");
        getName();
        while(true){
            getMsg();
            if(message.toUpperCase().compareTo("Q")==0){
                Client.close();
                break;
            }
            byte[] msg = message.getBytes();
            ByteBuffer buffer = ByteBuffer.wrap(msg);
            Client.write(buffer);

            show("sending " + message + " from " + clientName);

            buffer.clear();
            Thread.sleep(2000);

        }
}

    private static void show(String str) {
        System.out.println(str);
    }

    private static void getName() {
        show("Enter the client name: ");
        Scanner sc = new Scanner(System.in);
        clientName = sc.nextLine();
    }

    private static void getMsg() {
        show("Enter the message");
        Scanner sc = new Scanner(System.in);
       message = sc.nextLine();

    }
}