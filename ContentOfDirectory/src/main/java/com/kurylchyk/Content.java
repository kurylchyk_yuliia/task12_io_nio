package com.kurylchyk;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Content {
    private File directory;
    private File[] contentOfDirectory;

    public void setDir(File dir) {
         this.directory = dir;
        this.contentOfDirectory = directory.listFiles();
    }

    /**
     * Shows all the files
     */
   public List<String>  showFiles() {

       List<String> allFiles = new ArrayList<String>();
       System.out.println("All the files!");
        for(File element :contentOfDirectory) {
            if(element.isFile()){
                allFiles.add(element.toString());
            }
        }
        return allFiles;
   }

    /**
     * Shows all the subdirectories
     */
   public List<String> showSubDir(){
       System.out.println("All the subdirectories");
       List<String> allDir = new ArrayList<String>();
       for(File element :contentOfDirectory) {
           if(element.isDirectory()) {
               allDir.add(element.toString());
           }
       }
       return allDir;
   }

   public File[] getContentOfDirectory(){
       return contentOfDirectory;
   }
}
