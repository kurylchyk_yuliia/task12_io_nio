package com.kurylchyk;

import java.io.File;
import java.util.List;
import java.util.Scanner;

public class Controller {
    private View view;
    private Content content;
    private String currentDir;
    private static Scanner sc = new Scanner(System.in);

    public Controller() {
        view = (s) -> {
            System.out.println(s);
        };
        content = new Content();
        setFirstDir();

        chooseDir();
    }


    /**
     * Setting the first dir
     */
    public void setFirstDir() {
        view.show("Enter the name of the dir :");
        currentDir = sc.nextLine();
        File dir = new File(currentDir);
        content.setDir(dir);
    }

    /**
     * Shows all the files in file
     */
    public void getFiles() {
        List<String> allFiles = content.showFiles();
        for (String element : allFiles) {
            view.show(element);
        }
    }

    /**
     * Shows all the directories in file
     */
    public void getDir() {
        List<String> allDir = content.showSubDir();
        for (String element : allDir) {
            view.show(element);
        }
    }

    public void setDir(String str) {
        boolean isHere = false;
        String newDir = currentDir + "\\\\" + str;
        currentDir = newDir;

        File[] allFiles = content.getContentOfDirectory();
        File newF = new File(currentDir);
        for(File element: allFiles) {
            if (newF.toString().compareTo(element.toString())==0) {
                content.setDir(newF);
                isHere = true;
            }
        }
        if(!isHere){
            view.show("Wrong dir!");
            view.show("Try again");
        }
    }

    public void chooseDir() {
        while (true) {
            String dir;
            getFiles();
            getDir();
           view.show("Enter the next dir");
            dir = sc.nextLine();
            if(dir.toUpperCase().compareTo("Q")==0){
                view.show("Finished");
                System.exit(0);
            }
            setDir(dir);
        }

    }
}
