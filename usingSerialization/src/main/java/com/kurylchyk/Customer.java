package com.kurylchyk;
import java.io.Serializable;

public class Customer implements Serializable {

    private String name;
    private String surname;
    private  transient String phoneNumber;
    private int age;
    private Address address;

    Customer(String name,
             String surname,
             String phone,
             int age,
             String city,
             String street,
             String building) {


        this.name = name;
        this.surname = surname;
        phoneNumber = phone;
        this.age = age;
        address = new Address(city, street,building);
    }


    @Override
    public String toString() {
        return "Customer:\t"+name+" "+surname + " "+age +" years old\n" +
                "Phone number:\t"+phoneNumber+ address.toString();
    }
}
