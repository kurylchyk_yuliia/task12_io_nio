package com.kurylchyk;
import java.io.*;

public class Application {

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        Customer customer = new Customer("Yullia","Kurylchyk","+3809310923456",19,"Lviv","Naukova","14A");

        System.out.println("Before serialization:");
        System.out.println(customer);
        ObjectOutputStream objectOutputStream =
                new ObjectOutputStream( new FileOutputStream("customer.out"));
        objectOutputStream.writeObject(customer);
        objectOutputStream.close();

        System.out.println("After serialization: ");

        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("customer.out"));
        Customer customerAfterSer = (Customer) objectInputStream.readObject();

        System.out.println(customerAfterSer);
    }

}
