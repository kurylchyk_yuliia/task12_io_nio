package com.kurylchyk;
import java.io.Serializable;

public class Address implements Serializable {

    private String city;
    private String street;
    private String building;


    public Address(String city, String street, String building){
        this.city = city;
        this.street = street;
        this.building = building;
    }


    @Override
    public String toString(){
        return "\nAddress:\t"+street+"\t" +building+"\t"+city;
    }
}
